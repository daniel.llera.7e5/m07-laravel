<?php

namespace App\Http\Controllers;

use App\Models\Menus;
use Illuminate\Http\Request;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus= Menus::Paginate(5);
        return view('client.menus')
        ->with('menus', $menus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
            return view('form2');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:15',
            'preu' => 'required|gte:10'
        ]);

        $menus=Menus::create($request->only('name','preu','comments'));

        return redirect()->route('menus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menus  $menus
     * @return \Illuminate\Http\Response
     */
    public function show(Menus $menus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menus  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menus $menu)
    {
        return view('form2')
        ->with('menu' , $menu);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Menus  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menus $menu)
    {
        $request->validate([
            'name' => 'required|max:15',
            'preu' => 'required|gte:10'
        ]);

        $menu->name = $request['name'];
        $menu->preu = $request['preu'];
        $menu->comments = $request['comments'];
        $menu->save();

        return redirect()->route('menus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menus  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menus $menu)
    {
        $menu->delete();
        return redirect()->route('menus.index');
 
    }
}
