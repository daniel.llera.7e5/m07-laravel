@extends('theme/base')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
@section('contingut')
<div class="row">
        <nav class="navbar navbar-expand-lg navbar-expand-md navbar-light bg-dark">
            <img style="width:100px;height:100px;" src="https://cdn.shortpixel.ai/spai/w_788+q_lossless+ret_img+to_webp/https://www.sosfactory.com/wp-content/uploads/2016/12/restaurant-logo-mr-bolat.png" alt="">

            <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon bg-light"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a style="font-size:16px;" class="font-size-5 nav-link text-white" href="{{ route('empleats.index')}}">Empleats</a>
                    </li>
                    <li class="nav-item">
                        <a style="font-size:16px;" class="font-size-5 nav-link text-white" href="{{ route('menus.index')}}">Menus</a>
                    </li>
                </ul>
            </div>
    </div>
<div class="container py-5 text-center">
   <h1>Llistat d'empleats</h1>
   <a href="{{ route('empleats.create')}}" class="btn btn-primary">Crear Empleat</a>
</div>

<table class="table">
  <thead>
    <tr>
      <th scope="col">nom</th>
      <th scope="col">saldo</th>
      <th scope="col">Accions</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($empleat as $item)
    <tr>
      <td>{{$item->name}}</td>
      <td>{{$item->due}}</td>
      <td><a href="{{route('empleats.edit', $item)}}" class="btn btn-warning">Editar</a> 
      <form action="{{ route('empleats.destroy', $item) }}" method="post" class="d-inline"> 
        @method('DELETE')
        @csrf
        <button type="submit" class="btn btn-danger">Eliminar</button>
      </form></td>
    </tr>
      @empty
      <td colspan="3">Actualment no hi ha registres</td>
      @endforelse
  </tbody>
</table>
{{$empleat->links("pagination::bootstrap-4")}}
@stop
</body>
</html>