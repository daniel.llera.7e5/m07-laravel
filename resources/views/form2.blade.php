@extends('theme/base')

@section('contingut')
<div class="container py-5 text-center">
    @if (isset($menu))
        <h1>Editar-Menu</h1>
    @else
   <h1>Crear Menus</h1>
   @endif

   @if (isset($menu))
    <form action="{{ route('menus.update', $menu)}}" method="post">
                @method('PUT')
   @else
   <form action="{{ route('menus.store')}}" method="post">
    @endif
    @csrf
   <div class="mb-3">
       <label for="name" class="form-label">Nom Menu</label>
       <input type="text" name="name" class="form-control" placeholder="Nom del Menu" value="{{old('name') ?? @$menu->name}}"
        step="0,01">   
       @error('name')
           <p class="form-text text-danger">{{$message}}</p>
       @enderror
    </div>
    <div class="mb-3">
       <label for="preu" class="form-label">Preu</label>
       <input type="number" name="preu" class="form-control" placeholder="Preu del Menu" value="{{old('preu') ?? @$menu->preu}}"
        step="0,01">
       @error('preu')
           <p class="form-text text-danger">{{$message}}</p>
       @enderror
   </div>

   <div class="mb-3">
       <label for="comments" class="form-label">Comentaris</label>
       <textarea type="text" name="comments" class="form-control" placeholder="Comentaris del Menu" step="0,01">{{old('comments') ?? @$menu->comments}}</textarea> 
       @error('comments')
           <p class="form-text text-danger">{{$message}}</p>
       @enderror 
   </div>

   @if (isset($menu))
   <button type="submit" class="btn btn-info">Editar Menu</button>
   @else
   <button type="submit" class="btn btn-info">Desar Menu</button>
   @endif

</form>

   
   
</div>
  
@endsection
