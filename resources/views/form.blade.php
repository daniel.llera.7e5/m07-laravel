@extends('theme/base')

@section('contingut')
<div class="container py-5 text-center">
    @if (isset($empleat))
        <h1>Editar-Empleat</h1>
    @else
   <h1>Crear empleats</h1>
   @endif

   @if (isset($empleat))
    <form action="{{ route('empleats.update', $empleat)}}" method="post">
                @method('PUT')
   @else
   <form action="{{ route('empleats.store')}}" method="post">
    @endif
   @csrf
   <div class="mb-3">
       <label for="name" class="form-label">Nom</label>
       <input type="text" name="name" class="form-control" placeholder="Nom de l'empleat" value="{{old('name') ?? @$empleat->name}}"
        step="0,01">   
       @error('name')
           <p class="form-text text-danger">{{$message}}</p>
       @enderror
    </div>
   <div class="mb-3">
       <label for="due" class="form-label">Salari</label>
       <input type="number" name="due" class="form-control" placeholder="Salari de l'empleat" value="{{old('due') ?? @$empleat->due}}"
        step="0,01">
       @error('due')
           <p class="form-text text-danger">{{$message}}</p>
       @enderror
   </div>
   <div class="mb-3">
       <label for="comments" class="form-label">Comentaris</label>
       <textarea type="text" name="comments" class="form-control" placeholder="Comentaris de l'empleat" step="0,01">{{old('comments') ?? @$empleat->comments}}</textarea> 
       @error('comments')
           <p class="form-text text-danger">{{$message}}</p>
       @enderror 
   </div>

   @if (isset($empleat))
   <button type="submit" class="btn btn-info">Editar Empleat</button>
   @else
   <button type="submit" class="btn btn-info">Desar Empleat</button>
   @endif

</form>
</div>
  
@endsection
