@extends('theme/base')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="https://kit.fontawesome.com/e3989053af.js"></script>

</head>

<body class="bg-secondary">
    @section('contingut')
    <div class="row">
        <nav class="navbar navbar-expand-lg navbar-expand-md navbar-light bg-dark">
            <img style="width:100px;height:100px;" src="https://cdn.shortpixel.ai/spai/w_788+q_lossless+ret_img+to_webp/https://www.sosfactory.com/wp-content/uploads/2016/12/restaurant-logo-mr-bolat.png" alt="">

            <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon bg-light"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a style="font-size:16px;" class="font-size-5 nav-link text-white" href="{{ route('empleats.index')}}">Empleats</a>
                    </li>
                    <li class="nav-item">
                        <a style="font-size:16px;" class="font-size-5 nav-link text-white" href="{{ route('menus.index')}}">Menus</a>
                    </li>
                </ul>
            </div>
    </div>

    <div class="col-lg-12 text-center bg-secondary">
        <h1 class=" text-center display-2 text-warning ">Restaurante</h1>
        <h4 class="font-weight-light  text-warning">
            Esta web es exclusiva para el administrador del restaurante donde puede Añadir,Actualizar y eliminar Empleados y Menus
        </h4>
       
    </div>

    <section>
        <div class="p-5 row justify-content-center  jumbotron jumbotron-fluid jumbo bg-secondary " style="background-color: #e7e4e4;">
            <div class="row text-center ">
                <div class="cards col-lg-6 col-sm-12 col-12card mt-5">
                    <i class="fas fa-5x text-center text-info fa-user-friends"></i>
                    <h2 class="card-title text-center">Empleados</h2>
                    <a style="font-size:16px;" class="fmb-4 btn btn-warning btn-lg px-4 m-2" href="{{ route('empleats.index')}}">Empleados</a>
                </div>

                <div class="cards col-lg-6 col-sm-12 col-12card mt-5">
                    <i class="fas fa-5x text-center text-info fa-utensils"></i>
                    <h2 class="card-title text-center">Menus</h2>
                    <a style="font-size:16px;" class="fmb-4 btn btn-warning btn-lg px-4 m-2" href="{{ route('menus.index')}}">Menus</a>
                </div>

            </div>
    </section>
    @stop

</body>

</html>